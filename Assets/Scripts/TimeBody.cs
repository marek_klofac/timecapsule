﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBody : MonoBehaviour
{
    public float recordTime = 5f;
	public int maxRewinds = 1;

    private bool isRewinding = false;
	private List<PointInTime> pointsInTime;
	private Rigidbody2D rb;
	private int usedRewinds = 0;

	// Use this for initialization
	void Start ()
    {
		pointsInTime = new List<PointInTime>();
		rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKeyDown(KeyCode.F))
			StartRewind();
		if (Input.GetKeyUp(KeyCode.F))
			StopRewind();
	}

	void FixedUpdate ()
	{
		if (isRewinding)
			Rewind();
		else
			Record();
	}

	void Rewind ()
	{
		if (pointsInTime.Count > 0)
		{
			PointInTime pointInTime = pointsInTime[0];
            transform.position = pointInTime.position;
			transform.rotation = pointInTime.rotation;
			pointsInTime.RemoveAt(0);
		}
        else
		{
			StopRewind();
		}
		
	}

	void Record ()
	{
		if (pointsInTime.Count > Mathf.Round(recordTime / Time.fixedDeltaTime))
		{
			pointsInTime.RemoveAt(pointsInTime.Count - 1); // Remove last point
		}
		pointsInTime.Insert(0, new PointInTime(transform.position, transform.rotation)); // Inserting new point at the beginning
	}

	public void StartRewind ()
	{
		if(usedRewinds < maxRewinds)
		{
			isRewinding = true;
			usedRewinds++;
			rb.isKinematic = true;
			// If object is player duplicate him (alternate reality)
			if(this.gameObject == GameManager.Instance.Player)
			{
				GameObject AR_player = Instantiate(this.gameObject) as GameObject;
				AR_player.name = "AR_player";
				AR_player.GetComponent<TimeBody>().enabled = false;
				AR_player.GetComponent<PlayerController>().enabled = false;
				AR_player.GetComponent<Animator>().enabled = false;
				AR_player.transform.position = gameObject.transform.position;
				AR_player.layer = LayerMask.NameToLayer("Ground");
				SpriteRenderer [] spriteRenderers = AR_player.GetComponentsInChildren<SpriteRenderer>();
				foreach (var spriteRenderer in spriteRenderers)
				{
					spriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, 0.25f);
				}
			}
		}
	}

	public void StopRewind ()
	{
		isRewinding = false;
		rb.isKinematic = false;
	}
}
