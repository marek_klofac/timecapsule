﻿using UnityEngine.Assertions;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] GameObject player = null;

    public GameObject Player{
        get{return player;}
    }

    public void LevelFinished()
    {
        player.GetComponent<PlayerController>().enabled = false;
        player.GetComponent<TimeBody>().enabled = false;
        player.GetComponent<Animator>().SetBool("IsWalking", false);
    }

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(player);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
