﻿using UnityEngine;
using System;

public class PlayerController : Singleton<PlayerController>
{
    public float speed;
    public float jumpForce;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;
    public int extraJumpValue;
    
    private int extraJumps;
    private float moveInput;
    private Rigidbody2D rb;
    private Animator anim;
    private bool facingRight = true;
    private bool isGrounded;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        extraJumps = extraJumpValue;
        if(gameObject.transform.Find("Model") == null)
        {
            throw new Exception("Player hierarchy doesn't contain 'Model' structure with sprites.");
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);

        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
        if(moveInput == 0)
        {
            anim.SetBool("IsWalking", false);
        }
        else
        {
            anim.SetBool("IsWalking", true);
            anim.Play("Walk");
        }

        if(facingRight == false && moveInput > 0)
        {
            Flip();
        }
        else if(facingRight == true && moveInput < 0)
        {
            Flip();
        }
    }

    void Update()
    {
        if(isGrounded == true)
        {
            extraJumps = extraJumpValue;
        }
        if((Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space)) && extraJumps > 0)
        {
            rb.velocity = Vector2.up * jumpForce;
            extraJumps--;
            anim.Play("Jump");
        }
        else if((Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space)) && extraJumps == 0 && isGrounded == true)
        {
            rb.velocity = Vector2.up * jumpForce;
            anim.Play("Jump");
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "EndOfLevel")
        {
            GameManager.Instance.LevelFinished();
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
       if(other.gameObject.tag == "PressurePad")
        {
            other.transform.root.GetComponent<PressurePadController>().Pressed();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
       if(other.gameObject.tag == "PressurePad")
        {
            other.transform.root.GetComponent<PressurePadController>().notPressed();
        }
    }

    private void Flip()
    {
        facingRight = !facingRight;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }
}
