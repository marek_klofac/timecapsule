﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class PressurePadController : MonoBehaviour
{
    [SerializeField] GameObject doorToOpen = null;
    private Animator doorAC;
    private Animator anim;

    public void Pressed()
    {
        anim.SetBool("IsPressed", true);
        doorAC.SetBool("IsOpened", true);
    }

    public void notPressed()
    {
        anim.SetBool("IsPressed", false);
        doorAC.SetBool("IsOpened", false);
    }

    private void Awake()
    {
        Assert.IsNotNull(doorToOpen);
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        doorAC = doorToOpen.GetComponent<Animator>();
    }
}
